//
//  ViewController.swift
//  ios2910ls13
//
//  Created by WA on 12/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import WebKit
import SafariServices

class ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        webView.load(URLRequest(url: URL(string: "https://www.youtube.com/watch?v=JWY-WNxRpu4")!))
//        webView.loadFileURL(<#T##URL: URL##URL#>, allowingReadAccessTo: <#T##URL#>)
//        let safariVC =
//        SFSafariViewController(url: URL(string: "https://www.youtube.com/watch?v=JWY-WNxRpu4")!)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 4) { [weak self] in
//            self?.present(safariVC, animated: true, completion: nil)
//        }
//        let items = [URL(string: "https://www.apple.com")!]
////        UIActivity
//        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
//        ac.excludedActivityTypes = [.postToFacebook, .airDrop]
//        present(ac, animated: true)
//        UIActivityViewController(activityItems: <#T##[Any]#>, applicationActivities: )
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let items = [URL(string: "https://www.apple.com")!]
//        let items = [UIImage(named: "cda")]
        //        UIActivity
                let ac = UIActivityViewController(activityItems: [self], applicationActivities: nil)
                ac.excludedActivityTypes = [.postToFacebook]
                present(ac, animated: true)
    }

}

extension ViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "I am ViewController"
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        guard let activityType = activityType else { return nil }
        switch activityType {
        case .airDrop:
            return "Ouch AirDrop"
        case .copyToPasteboard:
            return "Ouch you sheety bastard"
        default:
            return "Will"
        }
//        return "ViewController I am"
    }
}
