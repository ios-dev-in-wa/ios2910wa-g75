//
//  ViewController.swift
//  ios2910ls3
//
//  Created by WA on 11/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

//typealias Name = String
//typealias OptionalCallback = (() -> Void)?

class ViewController: UIViewController {
    
    var someBool = true

    var someThing: String {
        return someBool ? "OMG" : "FYI"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        let callback: OptionalCallback = { }
//        let userName: Name = ""
//        print("\(greeting(name: "Artem")) \(greeting(name: "Valera"))")
//
//        for _ in 0...5 {
//            ternary(money: Int.random(in: 0...200))
//        }
//        let someFunc = ternary(money: 10)

//        let ternaryFunc = ternanryFunction
//        ternaryFunc
//        ternaryFunc()
//        ternaryFunc()
//        ternaryFunc
//
//        ternanryFunction {
//            print("I am completed2")
//        }
//        print("I am completed")

//        true ? ternary(money: 100) : ternary(money: 200)
//        var currentTheme = ThemeStyle.day

//        switch currentTheme {
//        case .day:
//            print("DAY")
//        case .night:
//            print("NIGHT")
//        }
//        print(getUserByNumber(number: 1))
//        print(getUserByNumber(number: 0))
//        print(getUserByNumber(number: 5))
//
//        var isSaved = false
//
//        if !isSaved {
//            print("Hello i am !")
//        }
//        stringExamples()
//        dictionaryExample()
//        print(someThing)
//        someBool.toggle()
//        print(someThing)
//        square(number: 9)
    }

    func greeting(name: String) -> String {
        let helloString = "Hello"
        return "\(helloString), dear: \(name)"
    }

    func addSomeMath(double: Double, secondDouble: Double) -> Double {
        double / secondDouble
    }

    func ternary(money: Int) {
//        if money > 100 {
//            print("Rich")
//        } else {
//            print("Poor")
//        }
        print( money > 100 ? "Rich" : "Poor")
    }

    func ternanryFunction(completionHandler: @escaping () -> Void) {
        print("ternaryFunc")
        print("ternaryFunc")
        print("ternaryFunc")
        print("ternaryFunc")
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            completionHandler()
        }
    }

    enum ThemeStyle {
        case day
        case night
    }

    func getUserByNumber(number: Int) -> String {
        switch number {
        case 0: return "Irina"
        case 1: return "Anton"
        case 2: return "Igor"
        default: return "Unnamed"
        }
//        if number == 0 {
//            return "Irina"
//        } else if number == 1 {
//            return "Anton"
//        } else if number == 2 {
//            return "Igor"
//        }
//        return "Unnamed"
    }

    func stringExamples() {
//        let some: () -> Void = { }
        var arrayOfStrings = ["String", "Second", "Third"]
        print(arrayOfStrings, arrayOfStrings.count)
        arrayOfStrings.append("Nikto")
        print(arrayOfStrings, arrayOfStrings.count)
        arrayOfStrings.insert("Noone", at: 1)
        print(arrayOfStrings, arrayOfStrings.count)

        arrayOfStrings.append(contentsOf: ["Artem", "Computer"])
        print(arrayOfStrings, arrayOfStrings.count)

        arrayOfStrings.forEach { element in
            printElement(element: element)
        }

        let filteredArray = arrayOfStrings.filter { element -> Bool in
            return element.contains("t")
        }

        let lowercasedArray = arrayOfStrings.filter { $0.lowercased().contains("t") }
        print(filteredArray)
        print(lowercasedArray)
//        print(arrayOfStrings.sort())
//        arrayOfStrings.sort { elementOne, elementTwo -> Bool in
//            return elementOne.count > elementTwo.count
//        }

//        arrayOfStrings.sort(by: <)
//        arrayOfStrings.sort(by: { $0.count < $1.count })
//        arrayOfStrings.reverse()
//        print(arrayOfStrings)
        var arrayOfInts = [1, 45, 6, 23, 88]

        let val = arrayOfInts.first(where: { $0 == 6} )
        
//        arrayOfInts
//        for element in arrayOfStrings {
//            printElement(element: element)
//        }
//        name.
        let name = "Artem"
        print(name.hasPrefix("Ar"))
        print(name.replacingOccurrences(of: "Ar", with: "Pt"))

        let someNewVar = ""

        print(arrayOfInts[2], arrayOfInts[1])

    }

    func printElement(element: String) {
        print(element)
    }
    
    func maxNumber(first: Int, second: Int) {
//        max(first, second)
        let result = [first, second].max()! //инициализируем мы, по этому можно
        print("Max number of \(first) and \(second) is: \(result)")
    }

    func square(number: Double) {
        let result = sqrt(number)
        print("Square of \(number) is: \(Int(result))")
    }

    func dictionaryExample() {
        let userFriends: [String: [String]] = [
            "Artem": ["Dima", "Sergey", "Illya"],
            "Nikita": ["Anton", "Nina", "Oleg", "Artem"],
            "Anna": ["Egor"]
        ]

        print(userFriends)

        print(userFriends["Nikita"])

        print(userFriends.keys)

        print(userFriends.values)

//        userFriends.
    }
}

