//
//  AppDelegate.swift
//  ios2910ls11
//
//  Created by WA on 12/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        initParse()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    private func initParse() {
           let parseConfig = ParseClientConfiguration {
               $0.applicationId = "XqbX1NXZelSTBzZcDSnd9khx4eIvRt0QtFLGsNtz"
               $0.clientKey = "GJdf5QBFxVKc7BuGfYd9vp0c7sEfNZrkjIF0O0Br"
               $0.server = "https://parseapi.back4app.com"
           }
           Parse.initialize(with: parseConfig)
       }
}

