//
//  AnimationViewController.swift
//  ios2910ls11
//
//  Created by WA on 12/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {

    @IBOutlet weak var animatedView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func randomAnimate(_ sender: UIBarButtonItem) {
//        UIView.animate(withDuration: 1) {
//            self.animatedView.backgroundColor = .random
//            self.animatedView.frame.size.height = CGFloat.random(in: 10...200)
//            self.animatedView.frame.size.width = CGFloat.random(in: 10...200)
//            self.animatedView.center = CGPoint(x: CGFloat.random(in: 10...200), y: CGFloat.random(in: 10...200))
//            self.animatedView.alpha = CGFloat.random(in: 0...1)
//            self.animatedView.layer.cornerRadius = CGFloat.random(in: 10...600)
//        }
//        UIView.animate(withDuration: 2.0, delay: 1.0, options: [.curveLinear], animations: {
//            self.animatedView.backgroundColor = .random
//        }) { completed in
//            print("COMPLETED")
//        }
//        let affine = CGAffineTransform()
//        affine.rotated(by: 5.0)
//        let some = { print("SSSS") }
        UIView.animate(withDuration: 1, animations: {
            
            self.animatedView.transform = CGAffineTransform(translationX: -10, y: -50).rotated(by: 5.0).scaledBy(x: 5, y: 5)
            self.animatedView.backgroundColor = .random
        }, completion: { completed in
            self.animatedView.transform = .identity
        })
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}
