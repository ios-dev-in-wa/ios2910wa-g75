//
//  ViewController.swift
//  ios2910ls11
//
//  Created by WA on 12/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import AVKit
import Parse

class ViewController: UIViewController {

    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var panView: UIView!

    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFields()

        let query = PFQuery(className:"Car")

        do {
            let objects = try query.findObjects()
            print(objects)
        } catch {
            print(error.localizedDescription)
        }
//        query.getObjectInBackgroundWithId("<PARSE_OBJECT_ID>") {
//          (parseObject: PFObject?, error: NSError?) -> Void in
//          if error == nil && parseObject != nil {
//            print(parseObject)
//          } else {
//            print(error)
//          }
//        }
    }

    func playSound() {
        guard let url = Bundle.main.url(forResource: "sound", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }

    @IBAction func loginAction(_ sender: UIBarButtonItem) {
        let vc = PFLogInViewController()
        // to add custom logo
//        vc.logInView?.logo
        present(vc, animated: true)
    }
    
    func setupFields() {
        textFields.forEach {
            $0.backgroundColor = .random
            $0.text = "\(Double.random(in: 0...10))"
        }
    }

//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesBegan(touches, with: event)
//        UIView.transition(with: view, duration: 0.5, options: [.transitionCurlUp], animations: {
//            self.setupFields()
//        }, completion: nil)
//    }
    @IBAction func didTapAction(_ sender: UITapGestureRecognizer) {
        playSound()
        switch sender.state {
        case .began:
            print("BEGAN")
        case .possible:
            print("POSSIBLE")
        case .changed:
            print("CHANGEd")
        case .ended:
            print("ended")
            UIView.transition(with: view, duration: 0.5, options: [.transitionCurlUp], animations: {
                self.setupFields()
            }, completion: nil)
        case .cancelled:
            print("cancelled")
        case .failed:
            print("failed")
        default:
            print("DEFAULT")
        }
    }

    @IBAction func didPanAction(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .began:
            print("BEGAN")
        case .possible:
            print("POSSIBLE")
        case .changed:
            panView.center = sender.location(in: view)
            print("CHANGEd")
            let some = sender.velocity(in: view)
            print(some)
        case .ended:
            print("ended")
            UIView.transition(with: view, duration: 0.5, options: [.transitionCurlUp], animations: {
                self.setupFields()
            }, completion: nil)
        case .cancelled:
            print("cancelled")
        case .failed:
            print("failed")
        default:
            print("DEFAULT")
        }
    }

    @IBAction func didPinchAction(_ sender: UIPinchGestureRecognizer) {
        print(sender.scale)
        
    }
    
    @IBAction func didSwipeAction(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .left:
            print("LEFT")
        case .right:
            print("RIGHT")
        default: fatalError("LOL")
        }
    }
    
    @IBAction func didRotationAction(_ sender: UIRotationGestureRecognizer) {
        switch sender.state {
        case .changed:
            UIView.animate(withDuration: 0.1) {
                self.panView.transform = CGAffineTransform(rotationAngle: sender.rotation)
            }
        default:
            print("")
        }
//        panView.transform3D = CATransform3D(m11: <#T##CGFloat#>, m12: <#T##CGFloat#>, m13: <#T##CGFloat#>, m14: <#T##CGFloat#>, m21: <#T##CGFloat#>, m22: <#T##CGFloat#>, m23: <#T##CGFloat#>, m24: <#T##CGFloat#>, m31: <#T##CGFloat#>, m32: <#T##CGFloat#>, m33: <#T##CGFloat#>, m34: <#T##CGFloat#>, m41: <#T##CGFloat#>, m42: <#T##CGFloat#>, m43: <#T##CGFloat#>, m44: <#T##CGFloat#>)
    }
}

