//
//  AddCarViewController.swift
//  ios2910ls11
//
//  Created by WA on 12/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import Parse

class AddCarViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var productionYearTextField: UITextField!
    @IBOutlet weak var waInAccidentSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didPressSave(_ sender: UIBarButtonItem) {
        let parseObject = PFObject(className: "Car")

        parseObject["title"] = titleTextField.text
        parseObject["productionYear"] = Int(productionYearTextField.text ?? "0")
        parseObject["wasInAccident"] = waInAccidentSwitch.isOn
        parseObject["owner"] = PFUser.current()

        parseObject.saveInBackground {
            (success: Bool, error: Error?) in
          if success {
            print("SUCCESS")
            // The object has been saved.
          } else {
            print(error?.localizedDescription)
            // There was a problem, check error.description
          }
        }
    }
    
}
