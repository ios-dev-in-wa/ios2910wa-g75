//
//  ViewController.swift
//  ios2910ls5
//
//  Created by WA on 11/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let heroOne = Hero(title: "Agon'", health: 10, damage: 20)
    let heroTwo = Hero(title: "Vodka", health: 10, damage: 15)
    var arena: Arena?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        do {
//            let container = try getAge(forName: "Artem")
//            print(container)
//        } catch {
//            if let myError = error as? MyError {
//                print(myError.localizedDescription)
//            }
//        }
//        do {
//            let container = try getAge(forName: "Artem2")
//            print(container)
//        } catch {
//            print(error.localizedDescription)
//        }

//        if let container = try? getAge(forName: "Artem") {
//            print(container)
//        }
//        print(convertStrToTranslite(string: "Привет, меня зовут Игорь"))
//        print(selectionByPattern(massiveOfStrings: ["Privet", "Privet ya", "NotPrivet", "Lol"], soughtForString: "privet"))
//        print(antimat(string: "Fuck, FUCK, FUCKER"))
        arena = Arena(spectators: 10, warriorOne: heroOne, warriorTwo: heroTwo)
        arena?.zrelishe()
    }

    enum MyError: Error, LocalizedError {
        case tooYoung, tooOld

        var localizedDescription: String {
            switch self {
            case .tooOld: return "Hey. You are too old."
            case .tooYoung: return "Hey. You are too young."
            }
        }
    }

    func getAge(forName: String) throws -> Int {
        if forName == "Artem" {
            throw MyError.tooYoung
        }
        return 1
    }

    func convertStrToTranslite(string: String) -> String {
        let dictionary = [
            "А" : "A",
            "Б" : "B",
            "В" : "V",
            "Г" : "G",
            "Д" : "D", "Е": "E", "Ж" : "J", "З" : "Z", "И" : "I", "Й" : "Y", "К" : "K", "Л": "L", "М" : "M", "Н" : "N", "О" : "O", "П" : "P", "Р" : "R", "С" : "S", "Т" : "T", "У" : "U", "Ф" : "F", "Х" : "H", "Ц" : "TS", "Ч" : "TSH", "Ш" : "SH", "Щ" : "SHTSH", "Ъ" : "", "Ы" : "Y", "Ь" : "'", "Э" : "E", "Ю" : "YU", "Я" : "YA", "а" : "a", "б" : "b", "в" : "v", "г" : "g", "д" : "d", "е": "e", "ж" : "j", "з" : "z", "и" : "i", "й" : "y", "к" : "k", "л": "l", "м" : "m", "н" : "n", "о" : "o", "п" : "p", "р" : "r", "с" : "s", "т" : "t", "у" : "u", "ф" : "f", "х" : "h", "ц" : "ts", "ч" : "tsh", "ш" : "sh", "щ" : "shtsh", "ъ" : "", "ы" : "y", "ь" : "'", "э" : "e", "ю" : "yu", "я" : "ya"
        ]
        var translitString = string
        for (key, value) in dictionary {
            translitString = translitString.replacingOccurrences(of: key, with: value)
        }
        return translitString
    }

    func selectionByPattern(massiveOfStrings: [String], soughtForString: String) -> [String] {
        var result: [String] = []
        for string in massiveOfStrings {
            if string.contains(soughtForString) {
                result += [string]
            }
        }
        return result
    }

    func antimat(string: String) -> String {
        let abusiveLanguage: Set<String> = ["fuck", "fak"]
        var result = string
        for i in abusiveLanguage {
            result = result.replacingOccurrences(of: i, with: "***")
        }
        return result
    }
}

