//
//  Hero.swift
//  ios2910ls5
//
//  Created by WA on 11/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class Hero {
    var health: Int
//    {
//        didSet(settedValue) {
//            print("[Info] \(title) \(settedValue)")
//            if settedValue <= 0 {
//                print("\(title) is DEAD.")
//            }
//        }
//    }
    var damage: Int
    var title: String

    init(title: String, health: Int, damage: Int) {
        self.title = title
        self.health = health
        self.damage = damage
    }

    func attack() -> Int {
        return Int.random(in: 1...damage)
    }

    func receive(damage: Int) -> Int {
        if Bool.random() {
            health -= damage
        }
        if health <= 0 {
            print(title + "id DEAD")
        }
        return health
    }
}
