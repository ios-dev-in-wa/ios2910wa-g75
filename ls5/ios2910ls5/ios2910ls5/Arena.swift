//
//  Arena.swift
//  ios2910ls5
//
//  Created by WA on 11/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class Arena {
    var spectators: Int
    var warriorOne: Hero
    var warriorTwo: Hero

    init(spectators: Int, warriorOne: Hero, warriorTwo: Hero){
        self.spectators = spectators
        self.warriorOne = warriorOne
        self.warriorTwo = warriorTwo
    }

    func zrelishe() {
        while warriorOne.health > 0 && warriorTwo.health > 0 {
            let damageOne = warriorOne.attack()
            let damageTwo = warriorTwo.attack()

            let resultTwo = warriorTwo.receive(damage: damageOne)
            if warriorTwo.health <= 0 {
                break
            }
            let resultOne = warriorOne.receive(damage: damageTwo)

            commentator(warriorOneTitle: warriorOne.title, warriorTwoTitle: warriorTwo.title, damage: damageOne, result: resultTwo)
            commentator(warriorOneTitle: warriorTwo.title, warriorTwoTitle: warriorOne.title, damage: damageTwo, result: resultOne)
        }
//        let deadHero = warriorOne.health <= 0 ? warriorOne : warriorTwo
//        let aliveHero = warriorOne.health <= 0 ? warriorTwo : warriorOne

//        applauseForWinner(heroTitle: aliveHero.title, deadOne: deadHero.title)
    }
    
//    func someIf(number: Int) {
//        if 0 < number {
//            return
//        }
//
//        print(number)
//    }

    private func commentator(warriorOneTitle: String, warriorTwoTitle: String, damage: Int, result: Int) {
        print("\(warriorOneTitle) prepared his attack for \(damage), and punish \(warriorTwoTitle) for \(result) HP")
    }

    private func applauseForWinner(heroTitle: String, deadOne: String) {
        for _ in 0...spectators {
            print("VIVA \(heroTitle)")
            print("HAHAHA \(deadOne)")
        }
    }
}
