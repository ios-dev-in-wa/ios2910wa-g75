//
//  MainViewController.swift
//  ios2910ls5
//
//  Created by WA on 11/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

struct TempExample {
    var name: String
}

class MainViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var tapMeButton: UIButton!

    let contantName = "My View Controller"
    var tapCounter = 0
//    var coffeMachine:
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // CLASS EXAMPLE
//        var hero = Hero(title: "Artem", health: 10, damage: 10)
//        print(hero.title)
//
//        let heroRef = hero
//
//        heroRef.title = "Fin"
//
//        print(hero.title)

        // STRUCT EXAMPLE
        var structOne = TempExample(name: "Tor")

        var structTwo = structOne

        structTwo.name = "Seled'"

        print(structTwo.name, structOne.name)
    }

    @IBAction func tapMeButtonAction(_ sender: UIButton) {
        print("TAPPED")
        tapCounter += 1
        sender.setTitle("Tap: \(tapCounter)", for: .normal)
        titleLabel.text = "Tapp control: \(tapCounter)"
        avatarImageView.image = UIImage(named: "image\(tapCounter)")
    }

//    func foo() {
//
//    }

    var waterContainer: Int = 200
    var coffeeContainer: Int = 30
    func prepareLatte() {
        if waterContainer <= 200 {
            print("Fill with water")
            return
        }
        if coffeeContainer <= 30 {
            print("Fill with coffee")
            return
        }
        waterContainer -= 200
        coffeeContainer -= 30
        print("Sir, your Latte is ready")
    }
}
