//
//  ViewController.swift
//  ios2910ls15
//
//  Created by WA on 12/17/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UserDefaults.standard.synchronize()
        let sliderValue = UserDefaults.standard.value(forKey: "slider_preference")
        print(sliderValue)
        let nameValue = UserDefaults.standard.value(forKey: "name_preference")
        print(nameValue)
    }


    @IBAction func didPressSettings(_ sender: UIBarButtonItem) {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)

    }
}

