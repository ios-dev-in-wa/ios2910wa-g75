//
//  SecondViewController.swift
//  ios2910ls15
//
//  Created by WA on 12/17/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        ImageService.getImage(withURL: URL(string: "https://steamuserimages-a.akamaihd.net/ugc/940586530515504757/CDDE77CB810474E1C07B945E40AE4713141AFD76/")!) { [weak self] image in
//            self?.imageView.image = image
//        }
        imageView.animationImages = [
            UIImage(named: "Poof-Frame0")!,
            UIImage(named: "Poof-Frame1")!,
            UIImage(named: "Poof-Frame2")!,
            UIImage(named: "Poof-Frame3")!,
            UIImage(named: "Poof-Frame4")!,
        ]
        imageView.animationDuration = 0.5
        imageView.startAnimating()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
