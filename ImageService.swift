import UIKit

class ImageService {

    static let isCacheEnabled = true

    static let cache = NSCache<NSString, UIImage>()

    static func downloadImage(withURL url: URL, completion: @escaping ((UIImage?) -> Void)) {
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    completion(nil)
                    print(error?.localizedDescription ?? "ERROR, \(url)")
                    return
            }
            DispatchQueue.main.async {
                if isCacheEnabled {
                    cache.setObject(image, forKey: url.absoluteString as NSString)
                }
                completion(image)
            }
            }.resume()
    }

    static func getImage(withURL url: URL, completion: @escaping ((UIImage?) -> Void)) {
        if let image = cache.object(forKey: url.absoluteString as NSString) {
            completion(image)
        } else {
            downloadImage(withURL: url, completion: completion)
        }
    }
}
