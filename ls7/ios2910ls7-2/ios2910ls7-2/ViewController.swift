//
//  ViewController.swift
//  ios2910ls7-2
//
//  Created by WA on 11/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let mutableString = NSMutableAttributedString(string: "")
            .bold("HEADER\n\n")
            .italic("Italic underline", color: .red)
            .bold("Ololo", color: .green)
        textView.attributedText = mutableString
    }


}

