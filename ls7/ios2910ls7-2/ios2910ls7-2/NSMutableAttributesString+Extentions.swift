
import UIKit

extension NSMutableAttributedString {
    
    @discardableResult func bold(_ text: String, color: UIColor = .black) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 17.5, weight: .bold),
            .foregroundColor: color
        ]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)

        return self
    }

    @discardableResult func normal(_ text: String, color: UIColor = .black) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key : Any] = [
            .font: UIFont.systemFont(ofSize: 16.5, weight: .medium),
            .foregroundColor: color
        ]
        let normal = NSMutableAttributedString(string:text, attributes: attrs)
        append(normal)

        return self
    }

    @discardableResult func italic(_ text: String, color: UIColor = .black) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key : Any] = [
            .font: UIFont.italicSystemFont(ofSize: 16.5),
            .foregroundColor: color,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let normal = NSMutableAttributedString(string:text, attributes: attrs)
        append(normal)

        return self
    }
}
