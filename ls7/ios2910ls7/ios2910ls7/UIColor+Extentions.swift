//
//  UIColor+Extentions.swift
//  ios2910ls7
//
//  Created by WA on 11/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit.UIColor

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
