//
//  ViewController.swift
//  ios2910ls7
//
//  Created by WA on 11/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            let data = try Data(contentsOf: getDocumentsDirectory().appendingPathComponent("User.txt"))
            let decoder = JSONDecoder()
            let user = try decoder.decode(User.self, from: data)
            print(user.name)
        } catch {
            print(error.localizedDescription)
        }
        navigationItem.title = "Login"
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case "goToCasino":
//            if let viewController = segue.destination as? CasinoViewController {
//
//            }
//            print("I AM")
            guard let viewController = segue.destination as? CasinoViewController else {
                print("I AM")
                return
            }
            viewController.currentVisitor = User(name: nameTextField.text ?? "Anonymous")
        default:
            break
        }
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let color = hexStringToUIColor(hex: "#ff8243")
        view.backgroundColor = .random
        descriptionLabel.font = .regular15
    }

    @IBAction func saveUser(_ sender: UIButton) {
        let user = User(name: nameTextField.text ?? "Anonymous")

        let encoder = JSONEncoder()
        do {
            let userData = try encoder.encode(user)
//            guard var url = URL(string: "file://Users/wa/Desktop/DataHolder") else { return }
            var documentUrl = getDocumentsDirectory()
            documentUrl.appendPathComponent("User.txt")
            try userData.write(to: documentUrl, options: .atomic)
            print(userData)
        } catch {
            print(error.localizedDescription)
        }
    }

    func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

