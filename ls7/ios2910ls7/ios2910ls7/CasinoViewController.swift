//
//  CasinoViewController.swift
//  ios2910ls7
//
//  Created by WA on 11/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class CasinoViewController: UIViewController {

    @IBOutlet weak var casinoSwitcher: UISegmentedControl!
    @IBOutlet weak var displayLabel: UILabel!
    @IBOutlet weak var moneyReceiverTextField: UITextField!

    private let casino = Casino()

    var currentVisitor: User?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Casino"
//        navigationItem.rightBarButtonItem = //UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshAction))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "TAP ME", style: .plain, target: self, action: #selector(refreshAction))

        
        // removes current view controller
//        dismiss(animated: true, completion: nil)
    }

    @objc
    func refreshAction() {
        print("Magic")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let name = currentVisitor?.name {
            navigationItem.title = "Hello, \(name)"
        }
    }
    @IBAction func krytiBarabanAction(_ sender: UIButton) {
        guard let text = moneyReceiverTextField.text,
            let money = Int(text),
            currentVisitor?.wallet ?? 0 >= money,
            let bidType = Casino.BidType(rawValue: casinoSwitcher.selectedSegmentIndex) else {
            displayLabel.text = "No money, no honey!"
            return
        }
        
        displayLabel.text = casino.makeABid(money: money, bidType: bidType)
    }
}
