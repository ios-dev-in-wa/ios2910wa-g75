//
//  User.swift
//  ios2910ls7
//
//  Created by WA on 11/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct User: Codable {
    let name: String
    var wallet: Int = 1000
}
