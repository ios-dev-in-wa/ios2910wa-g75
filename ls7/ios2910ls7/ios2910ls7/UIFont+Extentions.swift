//
//  UIFont+Extentions.swift
//  ios2910ls7
//
//  Created by WA on 11/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit.UIFont

extension UIFont {

    static var regular17: UIFont { return UIFont(name: "Tomorrow-Regular", size: 17)! }
    static var regular15: UIFont { return UIFont(name: "Tomorrow-Regular", size: 15)! }
    static var regular13: UIFont { return UIFont(name: "Tomorrow-Regular", size: 13)! }
    
}
