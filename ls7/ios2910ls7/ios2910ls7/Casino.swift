//
//  Casino.swift
//  ios2910ls7
//
//  Created by WA on 11/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class Casino {

    var lastWinningBid = BidType.red

    enum BidType: Int {
        case red = 0
        case black = 1
        case green = 2
    }
    
    func makeABid(money: Int, bidType: BidType) -> String {
        let winningBid = krytiBaraban()
        guard bidType == winningBid else {
            return "Loser"
        }
        switch bidType {
        case .black, .red:
            return "Your win is \(money * 2)"
        case .green:
            return "Your win is \(money * 100)"
        }
    }

    private func krytiBaraban() -> BidType {
        let val = Int.random(in: 0...100)
        var winningBid: BidType
        if val == 0 {
            winningBid = .green
        } else {
          winningBid = val % 2 == 0 ? .red : .black
        }
        return winningBid
    }
}
