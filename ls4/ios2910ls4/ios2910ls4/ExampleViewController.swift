//
//  ExampleViewController.swift
//  ios2910ls4
//
//  Created by WA on 11/7/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ExampleViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("HELLO I AM EXAMPLE")
        print(imageView.frame)
        print(imageView.bounds)
        
        view.sendSubviewToBack(imageView)
    }
}
