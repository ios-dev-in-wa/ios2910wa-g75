//
//  Car.swift
//  ios2910ls4
//
//  Created by WA on 11/7/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class Car {
    let numberOfWheels: Int
    let bodyColor: UIColor
    let bodyType: BodyType
    let engineVolume: Double
    private var engineTemperature: Int {
        return Int.random(in: 30...90)
    }

    enum BodyType {
        case cabri, coope, sedan, hb
    }

    init(numberOfWheels: Int, bodyColor: UIColor, bodyType: BodyType, engineVolume: Double) {
        self.numberOfWheels = numberOfWheels
        self.bodyColor = bodyColor
        self.bodyType = bodyType
        self.engineVolume = engineVolume
    }

    func move() {
        if engineTemperature < 50 {
            print("Move with speed 1")
        }
    }

    func stop() {
        
    }

    func rotate() {
        
    }
}
