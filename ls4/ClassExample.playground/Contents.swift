import UIKit

class Car {
    let numberOfWheels: Int
    let bodyColor: UIColor
    let bodyType: BodyType
    var engineVolume: Double

    enum BodyType {
        case cabri, coope, sedan, hb
    }

    init(numberOfWheels: Int, bodyColor: UIColor, bodyType: BodyType, engineVolume: Double) {
        self.numberOfWheels = numberOfWheels
        self.bodyColor = bodyColor
        self.bodyType = bodyType
        self.engineVolume = engineVolume
    }

    func move() {
        print("Move with speed 1")
    }

    func stop() {
        
    }

    func rotate() {
        
    }
}

enum BodyType {
    case cabri, coope, sedan, hb
}


let redCabry20 = Car(numberOfWheels: 4, bodyColor: .red, bodyType: .cabri, engineVolume: 2.0)
let greeenSedan15 = Car(numberOfWheels: 4, bodyColor: .green, bodyType: .sedan, engineVolume: 1.5)

print(redCabry20.engineVolume)

print(greeenSedan15.bodyColor)

class BattleCar: Car {
    let ammutionCounte: Int
    let gunType: GunType
    let squadQuantity: Int

    init(car: Car, ammutionCounte: Int, gunType: GunType, squadQuantity: Int) {
        self.ammutionCounte = ammutionCounte
        self.gunType = gunType
        self.squadQuantity = squadQuantity
        super.init(numberOfWheels: car.numberOfWheels, bodyColor: car.bodyColor, bodyType: car.bodyType, engineVolume: car.engineVolume)
    }

    init(numberOfWheels: Int, bodyColor: UIColor, bodyType: BodyType, engineVolume: Double, ammutionCounte: Int, gunType: GunType, squadQuantity: Int) {
        self.ammutionCounte = ammutionCounte
        self.gunType = gunType
        self.squadQuantity = squadQuantity
        super.init(numberOfWheels: numberOfWheels, bodyColor: bodyColor, bodyType: bodyType, engineVolume: engineVolume)
    }

    override func move() {
        super.move()
        print("Move with reverse speed 1")
    }
}

enum GunType {
    case turret, minigun, bazooka
}

let minigunPanzer = BattleCar(car: redCabry20, ammutionCounte: 100, gunType: .minigun, squadQuantity: 3)
let bazookaTank = BattleCar(numberOfWheels: 6, bodyColor: .black, bodyType: .hb, engineVolume: 10.0, ammutionCounte: 1000, gunType: .bazooka, squadQuantity: 5)

print(minigunPanzer.gunType)

print(bazookaTank.ammutionCounte)

bazookaTank.move()

bazookaTank.engineVolume = 10.0

