//
//  RandomUserService.swift
//  ios2910ls10
//
//  Created by WA on 11/28/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct RandomUserService {

    private let session = URLSession(configuration: .default)

    func getUsers(completion: @escaping ([User]) -> Void)  {
        guard let url = URL(string: "https://randomuser.me/api/?results=20") else { return }
        let request = URLRequest(url: url)
        // Adding HEADERS
//        request.addValue("MYTOKEN", forHTTPHeaderField: "Bearer Token")
        // Use to send DATA, where DATA is your struct
//        request.httpBody
        // TO SET httpMethod
//        request.httpMethod = ""
        // TIME OUT time 
//        request.timeoutInterval =
        
        let task = session.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let responseUnwrap = response as? HTTPURLResponse else { return }
            switch responseUnwrap.statusCode {
            case 200:
                guard let data = data else { return }
                let decoder = JSONDecoder()
                do {
                    let results = try decoder.decode(UserResult.self, from: data)
                    DispatchQueue.main.async {
                        completion(results.results)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            default:
                print("Server error")
            }
        }

        task.resume()
    }

    
}
