//
//  ProfileTableViewCell.swift
//  ios2910ls10
//
//  Created by WA on 11/28/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = nil
    }

    func setupWith(user: User) {
        profileImageView.downloaded(from: user.picture.large, contentMode: .scaleAspectFill)
        nameLabel.text = "\(user.name.title) \(user.name.first) \(user.name.last)"
        emailLabel.text = user.email
        genderLabel.text = user.gender
        nationalityLabel.text = user.nat
    }
}
