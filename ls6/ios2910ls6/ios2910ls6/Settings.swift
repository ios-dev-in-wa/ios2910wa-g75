//
//  Settings.swift
//  ios2910ls6
//
//  Created by WA on 11/14/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

final class Settings {

    static let shared = Settings()

    private init() { }

    private let defaults = UserDefaults.standard

    var isSoundEnabled: Bool {
        get {
            defaults.bool(forKey: "isSoundEnabled")
        }
        set(value) {
            defaults.set(value, forKey: "isSoundEnabled")
        }
    }
}
