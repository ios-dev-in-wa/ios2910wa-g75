//
//  ViewController.swift
//  ios2910ls6
//
//  Created by WA on 11/14/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLabel.text = Settings.shared.isSoundEnabled ? "ЗВУК ВКЛ" : "ЗВУК ВЫКЛ"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case "goToBrownVC":
            if let brownVC = segue.destination as? BrownViewController, let text = sender as? String {
                print(text)
                brownVC.delegate = self
                _ = brownVC.view // FORCE LOAD VIEW
                brownVC.textField.text = titleLabel.text
            }
        default: break
        }
    }

    @IBAction func forceGoAction(_ sender: UIButton) {
        performSegue(withIdentifier: "goToBrownVC", sender: "HELLO")
    }
}

extension ViewController: BrownVCDelegate {
    func didSave(title: String) {
        titleLabel.text = title
    }
}
