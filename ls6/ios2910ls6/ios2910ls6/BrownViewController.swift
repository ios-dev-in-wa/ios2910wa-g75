//
//  BrownViewController.swift
//  ios2910ls6
//
//  Created by WA on 11/14/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

protocol BrownVCDelegate: class {
    func didSave(title: String)
}

class BrownViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!

    weak var delegate: BrownVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        if let title = textField.text {
            delegate?.didSave(title: title)
        }
        Settings.shared.isSoundEnabled = !Settings.shared.isSoundEnabled
        navigationController?.popViewController(animated: true)
    }

    deinit {
        print("DID DEINIT BROWNVIEWCONTROLLER")
    }
}
