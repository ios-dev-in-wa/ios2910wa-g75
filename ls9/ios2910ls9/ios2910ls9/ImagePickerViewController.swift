//
//  ImagePickerViewController.swift
//  ios2910ls9
//
//  Created by WA on 11/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ImagePickerViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func imagePickerAction(_ sender: UIBarButtonItem) {
        let pickerVC = UIImagePickerController()
        pickerVC.sourceType = .camera
        pickerVC.delegate = self
        present(pickerVC, animated: true, completion: nil)
    }
}

extension ImagePickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        imageView.image = image
//        picker.dismiss(animated: true, completion: nil)
    }
}
