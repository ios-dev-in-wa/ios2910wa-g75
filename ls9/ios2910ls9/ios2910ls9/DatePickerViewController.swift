//
//  DatePickerViewController.swift
//  ios2910ls9
//
//  Created by WA on 11/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import DateScrollPicker

class DatePickerViewController: UIViewController {

    @IBOutlet weak var datePicker: DateScrollPicker!

    override func viewDidLoad() {
        super.viewDidLoad()

        var format = DateScrollPickerFormat()

        /// Number of days
        format.days = 5

        /// Top label date format
        format.topDateFormat = "EEE"

        /// Top label font
        format.topFont = UIFont.systemFont(ofSize: 10, weight: .regular)

        /// Top label text color
        format.topTextColor = UIColor.black

        /// Top label selected text color
        format.topTextSelectedColor = UIColor.white

        /// Medium label date format
        format.mediumDateFormat = "dd"

        /// Medium label font
        format.mediumFont = UIFont.systemFont(ofSize: 30, weight: .bold)

        /// Medium label text color
        format.mediumTextColor = UIColor.black

        /// Medium label selected text color
        format.mediumTextSelectedColor = UIColor.white

        /// Bottom label date format
        format.bottomDateFormat = "MMM"

        /// Bottom label font
        format.bottomFont = UIFont.systemFont(ofSize: 10, weight: .regular)

        /// Bottom label text color
        format.bottomTextColor = UIColor.black

        /// Bottom label selected text color
        format.bottomTextSelectedColor = UIColor.white

        /// Day radius
        format.dayRadius = 5

        /// Day background color
        format.dayBackgroundColor = UIColor.lightGray

        /// Day background selected color
        format.dayBackgroundSelectedColor = UIColor.darkGray

        /// Selection animation
        format.animatedSelection = true

        /// Separator enabled
        format.separatorEnabled = true

        /// Separator top label date format
        format.separatorTopDateFormat = "MMM"

        /// Separator top label font
        format.separatorTopFont = UIFont.systemFont(ofSize: 20, weight: .bold)

        /// Separator top label text color
        format.separatorTopTextColor = UIColor.black

        /// Separator bottom label date format
        format.separatorBottomDateFormat = "yyyy"

        /// Separator bottom label font
        format.separatorBottomFont = UIFont.systemFont(ofSize: 20, weight: .regular)

        /// Separator bottom label text color
        format.separatorBottomTextColor = UIColor.black

        /// Separator background color
        format.separatorBackgroundColor = UIColor.lightGray.withAlphaComponent(0.2)

        /// Fade enabled
        format.fadeEnabled = false

        /// Animation scale factor
        format.animationScaleFactor = 1.1

        /// Animation scale factor
        format.dayPadding = 5

        /// Top margin data label
        format.topMarginData = 10

        /// Dot view size
        format.dotWidth  = 10

        datePicker.format = format
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
