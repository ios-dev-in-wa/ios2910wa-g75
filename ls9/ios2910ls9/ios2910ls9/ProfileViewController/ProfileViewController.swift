//
//  ProfileViewController.swift
//  ios2910ls9
//
//  Created by WA on 11/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import SVProgressHUD

class ProfileViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!

    private let user = User(profileImage: "rick",
                            name: "Rick",
                            surname: "Sanchez",
                            dateOfBirth: "20.08.46",
                            adress: "NEVERMIND",
                            phoneNumber: "+666")

    override func viewDidLoad() {
        super.viewDidLoad()
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
        profileImageView.layer.borderWidth = 3
        profileImageView.layer.borderColor = UIColor.red.cgColor
        setupWithUser(user)
//        scrollView.contentInset = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //SVProgressHUD.show()
    }

    @IBAction func leftAction(_ sender: Any) {
        usernameLabel.removeFromSuperview()
    }
}

private extension ProfileViewController {
    func setupWithUser(_ user: User) {
        profileImageView.image = UIImage(named: user.profileImage)
        usernameLabel.text = user.name
        surnameLabel.text = user.surname
        dobLabel.text = user.dateOfBirth
        adressLabel.text = user.adress
        phoneNumberLabel.text = user.phoneNumber
    }
}
