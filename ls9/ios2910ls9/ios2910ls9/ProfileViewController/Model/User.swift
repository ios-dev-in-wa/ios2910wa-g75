//
//  User.swift
//  ios2910ls9
//
//  Created by WA on 11/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct User {
    let profileImage: String
    let name: String
    let surname: String
    let dateOfBirth: String
    let adress: String
    let phoneNumber: String
}
