//
//  ViewController.swift
//  gameOfTrones
//
//  Created by WA on 12/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var currentPage = 1
    private let pageSize = 20
    private var isLoading = false
    private let refreshControl = UIRefreshControl()

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }

    private let service = CharacterService()
    private var characters = [CharacterElement]()

    override func viewDidLoad() {
        super.viewDidLoad()
        updateList()
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        let picker = UIPickerView()
//        picker.dataSource = self
    }

    private func updateList() {
        isLoading = true
        service.getCharacters(page: currentPage, pageSize: pageSize) { [weak self] characaters in
            self?.characters.append(contentsOf: characaters)
            print(self?.characters.count ?? -1)
            DispatchQueue.main.async {
                self?.isLoading = false
                self?.refreshControl.endRefreshing()
                self?.tableView.reloadData()
            }
        }
        currentPage += 1
    }

    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        characters.removeAll()
        tableView.reloadData()
        currentPage = 1
        updateList()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == characters.count - 5, !isLoading else { return }
        updateList()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CharacherCell", for: indexPath)
        let currentCharacter = characters[indexPath.row]
        cell.textLabel?.text = currentCharacter.name.isEmpty ? currentCharacter.aliases.first ?? currentCharacter.url : currentCharacter.name
        cell.detailTextLabel?.text = currentCharacter.died
        return cell
    }
}
