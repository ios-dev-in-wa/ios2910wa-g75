//
//  CharacterService.swift
//  GameOfThrones
//
//  Created by WA on 12/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct CharacterService {

    private let session = URLSession.shared

    func getCharacters(page: Int, pageSize: Int, completion: @escaping (([CharacterElement]) -> Void)) {
        guard let url = URL(string: "https://www.anapioficeandfire.com/api/characters?page=\(page)&pageSize=\(pageSize)") else { return }
        session.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse, let data = data else { return }
            switch response.statusCode {
            case 200:
                let decoder = JSONDecoder()
                do {
                    let characters = try decoder.decode([CharacterElement].self, from: data)
                    completion(characters)
                } catch {
                    print(error.localizedDescription)
                }
            default: fatalError()
            }
        }
        .resume()
    }
}
