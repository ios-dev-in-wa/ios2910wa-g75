//
//  CharacterTableViewCell.swift
//  GameOfThrones
//
//  Created by WA on 12/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {

    override func prepareForReuse() {
        super.prepareForReuse()
        textLabel?.text = "OOOPS"
        detailTextLabel?.text = "OOOPSx2"
    }
    
}
