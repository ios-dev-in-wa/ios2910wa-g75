//
//  TextViewController.swift
//  GameOfThrones
//
//  Created by WA on 12/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import CoreTelephony
import AVKit

class TextViewController: UIViewController {
    
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    @IBOutlet weak var fifthTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    //    @IBOutlet weak var topConstraint: NSLayoutConstraint!
//    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        [firstTextField, secondTextField, thirdTextField, fourthTextField, fifthTextField].forEach {
            $0?.delegate = self
        }
        NotificationCenter.default.addObserver(self, selector: #selector(adjustKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        firstTextField.becomeFirstResponder()
    }

    @objc func adjustKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = .zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
    }

    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    @IBAction func showVideo(_ sender: UIBarButtonItem) {
        let player = AVPlayer(url: URL(string: "https://h5p.org/sites/default/files/h5p/iv.mp4")!)
        let vc = AVPlayerViewController()
        vc.player = player

        present(vc, animated: true) {
            vc.player?.play()
        }
    }
}

extension TextViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstTextField:
            secondTextField.becomeFirstResponder()
        case secondTextField:
            thirdTextField.becomeFirstResponder()
        case thirdTextField:
            fourthTextField.becomeFirstResponder()
        case fourthTextField:
            fifthTextField.becomeFirstResponder()
        case fifthTextField:
            fifthTextField.resignFirstResponder()
            // save data and send
            // save()
//            navigationController?.popViewController(animated: true)
        default:
            fatalError("NOT A GOOD TEXTFIELD")
        }
        return true
    }
}
