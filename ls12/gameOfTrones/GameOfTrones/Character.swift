//
//  Character.swift
//  GameOfThrones
//
//  Created by WA on 12/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct CharacterElement: Codable {
    let url: String
    let name: String
    let gender: Gender
    let culture: String
    let born: String
    let died: String
    let titles, aliases: [String]
    let father: String
    let mother: String
    let spouse: String
    let allegiances, books: [String]
    let povBooks: [String]
    let tvSeries, playedBy: [String]
}

enum Gender: String, Codable {
    case female = "Female"
    case male = "Male"
}
