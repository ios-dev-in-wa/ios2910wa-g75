//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L22H" //  Level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        //        turnLeft()
        
        //        moveIfFrontIsClear()
        //        moveIfFrontIsClear()
        //        moveIfFrontIsClear()
        //        moveIfFrontIsClear()
        //        moveIfFrontIsClear()
        //        moveIfFrontIsClear()
        //        moveIfFrontIsClear()
        //        moveIfFrontIsClear()
        
        whileExample()
        solveMyFirstTask()
    }
    
    //     bool, bool - AND
    //     bool && bool - AND
    //     bool || bool - OR
    func moveIfFrontIsClear() {
        if frontIsClear, facingLeft {
            doubleMove()
        } else  {
            turnRight()
        }
    }

    func solveMyFirstTask() {
        move()
        doubleMove()
        pick()
        turnRight()
        move()
        turnLeft()
        doubleMove()
        put()
        doubleMove()
    }
    
    /*
     func orExample() {
     if noCandyPresent || frontIsClear {
     move()
     } else {
     doubleMove()
     }
     }
     */
    
    func doubleMove() {
        move()
        move()
    }
    
    func turnLeft() {
        for _ in 0..<3 {
            turnRight()
        }
    }
    
    func whileExample() {
        while frontIsClear {
            if candyPresent {
                pick()
                break
            }
            move()
        }
    }
    
    
    
}

