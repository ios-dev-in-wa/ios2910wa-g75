//
//  AnimalTableViewCell.swift
//  ios2910ls8
//
//  Created by WA on 11/21/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class AnimalTableViewCell: UITableViewCell {

    @IBOutlet weak var animalImageView: UIImageView!
    @IBOutlet weak var animalTitleLabel: UILabel!
    @IBOutlet weak var animalDescriptionLabel: UILabel!

    func setupCell(model: Animal) {
        animalDescriptionLabel.text = model.description
        animalTitleLabel.text = model.title
        animalImageView.image = model.image
    }
}
