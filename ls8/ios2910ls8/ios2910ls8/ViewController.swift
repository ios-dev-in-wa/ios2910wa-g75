//
//  ViewController.swift
//  ios2910ls8
//
//  Created by WA on 11/21/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

let some = ViewController.Test(title: "SS")

class ViewController: UIViewController {

    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var redFace: FaceView!
    struct Test {
        let title: String
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        NotificationCenter.default.addObserver(self, selector: #selector(orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
//        detailLabel.text = "Logged in"
//        NotificationCenter.default.addObserver(self, selector: #selector(didPressLogoutButton), name: .didPressLogoutButton, object: nil)

        let date = Date()
//        detailLabel.text = "\(date.timeIntervalSince1970)"
//        date.
        let dateFormatter = DateFormatter()
//        dateFormatter.dateStyle = .full

        dateFormatter.dateFormat = "YYYY-MM-dd HH"
        detailLabel.text = dateFormatter.string(from: date)

        let stringDate = "1999-12-10 18"
        if let newDate = dateFormatter.date(from: stringDate) {
            dateFormatter.dateStyle = .full
            print(dateFormatter.string(from: newDate))
            let components = Calendar.current.dateComponents([.year, .weekday], from: newDate)
            print(components.weekday)
        }

        navigationController?.tabBarItem.title = "PEPE"
        navigationController?.tabBarItem.selectedImage = UIImage(named: "pig50")?.withRenderingMode(.alwaysOriginal)
//            .image = UIImage(named: )
//        print(dateFormatter.date(from: stringDate))
    }

//    @objc
//    func orientationDidChange() {
//        print("OrientationDidChange")
//    }

    @objc
    func didPressLogoutButton() {
        detailLabel.text = "Logged out"
    }
}

extension Notification.Name {
    static let didPressLogoutButton = Notification.Name("didPressLogoutButton")
}
