//
//  TableExampleViewController.swift
//  ios2910ls8
//
//  Created by WA on 11/21/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

struct Animal {
    let title: String
    let image: UIImage?
    let description: String
}

class TableExampleViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let animalsArray = [
       Animal(title: "Pig", image: UIImage(named: "pig50"), description: "PEPE"),
       Animal(title: "Wolf", image: nil, description: "WOOF"),
       Animal(title: "COW", image: nil, description: "MOOOOO"),
       Animal(title: "Pocket", image: UIImage(named: "pig50"), description: "PEPE"),
       Animal(title: "Purple", image: UIImage(named: "pig50"), description: "PEPE")
    ]

    var dictionary: [String: [Animal]] {
        Dictionary(grouping: animalsArray) { item -> String in
            String(item.title.first ?? "A")
        }
    }

    var sectionTitles: [String] {
        Array(dictionary.keys).sorted(by: { $0 < $1 })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self

        let nib = UINib(nibName: "NewCustomTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "SomeID")
    }
}

extension TableExampleViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}

extension TableExampleViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return dictionary.keys.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = sectionTitles[section]
        let valuesForKey = dictionary[key] ?? []
        return valuesForKey.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AnimalCell", for: indexPath) as? AnimalTableViewCell else { fatalError("AnimalCell should exist") }
        let key = sectionTitles[indexPath.section]
        guard let valuesForKey = dictionary[key] else { fatalError("NO CELLS for animals") }
        let model = valuesForKey[indexPath.row]
        cell.setupCell(model: model)
//        cell.imageView?.image = model.image
//        cell.animalTitleLabel.text = model.title
//        cell.animalDescriptionLabel.text = model.description
//        print(indexPath.section, indexPath.row)
//        cell.textLabel?.text = animalsArray[indexPath.row]
//        cell.detailTextLabel
//        cell.imageView
        return cell
    }
}
