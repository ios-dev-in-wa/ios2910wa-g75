//
//  ProfileViewController.swift
//  ios2910ls8
//
//  Created by WA on 11/21/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var faceView: FaceView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func logOutAction(_ sender: UIBarButtonItem) {
        NotificationCenter.default.post(name: .didPressLogoutButton, object: nil)
        navigationController?.popViewController(animated: true)
    }
}
