//
//  String+Extentions.swift
//  is2910ls14
//
//  Created by WA on 12/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
