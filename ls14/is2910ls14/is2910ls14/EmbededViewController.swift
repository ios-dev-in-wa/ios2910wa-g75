//
//  EmbededViewController.swift
//  is2910ls14
//
//  Created by WA on 12/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class EmbededViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let names = [
        "Artem",
        "Alexey",
        "Ivan"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension EmbededViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCoolCell", for: indexPath)
        cell.textLabel?.text = names[indexPath.row]
        return cell
    }
}
