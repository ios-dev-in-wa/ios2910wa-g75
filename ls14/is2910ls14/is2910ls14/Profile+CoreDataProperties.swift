//
//  Profile+CoreDataProperties.swift
//  is2910ls14
//
//  Created by WA on 12/12/19.
//  Copyright © 2019 WA. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var name: String?
    @NSManaged public var surname: String?
    @NSManaged public var id: UUID?

}
