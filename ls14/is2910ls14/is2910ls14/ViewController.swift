//
//  ViewController.swift
//  is2910ls14
//
//  Created by WA on 12/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var greetingLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        greetingLabel.text = "greetings.label".localized
    }


}

